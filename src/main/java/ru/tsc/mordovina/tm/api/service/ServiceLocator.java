package ru.tsc.mordovina.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

}
