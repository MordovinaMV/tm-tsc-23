package ru.tsc.mordovina.tm.command.system;

import com.sun.istack.internal.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandsDisplayCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "commands";
    }

    @Nullable
    @Override
    public String getArgument() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display list of commands";
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands)
            System.out.println(command.getCommand());

    }

}
