package ru.tsc.mordovina.tm.command.system;

import com.sun.istack.internal.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "exit";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Close application";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
