package ru.tsc.mordovina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractUserCommand;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.mordovina.tm.model.User;
import ru.tsc.mordovina.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserDisplayByLoginCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getCommand() {
        return "user-display-by-login";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display user by login";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getUserService().findUserByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        showUser(user);
    }

}
