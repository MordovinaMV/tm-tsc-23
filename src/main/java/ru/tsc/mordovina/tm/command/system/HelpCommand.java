package ru.tsc.mordovina.tm.command.system;

import com.sun.istack.internal.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "help";
    }

    @Nullable
    @Override
    public String getArgument() {
        return "-h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display list of commands";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands())
            System.out.println(command.toString());
    }

}
