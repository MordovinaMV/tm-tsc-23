package ru.tsc.mordovina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractOwnerEntity extends AbstractEntity {

    @Nullable
    protected String userId;

    @Nullable
    public String getUserId() {
        return userId;
    }

    public void setUserId(@Nullable final String userId) {
        this.userId = userId;
    }

}
