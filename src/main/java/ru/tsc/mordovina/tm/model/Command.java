package ru.tsc.mordovina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter

public final class Command {

    @NotNull private final String name;

    @Nullable private final String argument;

    @NotNull private final String description;

    public Command(@NotNull final String name, @Nullable final String argument, @NotNull final String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    @NotNull public String getDescription() {
        return description;
    }

    @NotNull public String getName() {
        return name;
    }

    @Nullable public String getArgument() {
        return argument;
    }

    @Override
    public String toString() {
        String result = "";
        result += name + " ";
        if (argument != null && !argument.isEmpty()) result += "(" + argument + ") ";
        result += description;
        return result;
    }

}
